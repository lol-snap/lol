# lol

`lol` is a variant of `snap` with support for a custom snap server. It's in beta, but you can daily-drive it. We'll be making it
easier to publish snaps and be adding support for multiple repos.

## Installation
Clone/download this repository and run `./lol install lol` in the cloned folder to install the `lol` snap after installing `snapd`.

## Upgrading
Run `lol refresh lol` to upgrade `lol` with `lol` (pun intended)

## Snap Store/Server
The default server is set to `https://lol-snap.gitlab.io/lol-server`. Run `lol repo` or `lol url` to add an `lol` repo. Here's the server repo which you can fork to set up your own repo: https://gitlab.com/lol-snap/lol-server
